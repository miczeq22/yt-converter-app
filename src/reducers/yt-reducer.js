import { SET_VIDEO_INFO, SET_VIDEO_INFO_ERROR, RESET_VIDEO_INFO } from '../actions/yt-actions';

const ytReducer = (state = { videoInfo: {}, videoInfoError: {} }, action = {}) => {
    switch (action.type) {
        case SET_VIDEO_INFO:
            return {
                ...state,
                videoInfo: action.videoInfo,
                videoInfoError: {},
            };

        case SET_VIDEO_INFO_ERROR:
            return {
                ...state,
                videoInfoError: action.videoInfoError,
            };

        case RESET_VIDEO_INFO:
            return {
                ...state,
                videoInfo: action.videoInfo,
                videoInfoError: action.videoInfoError,
            };

        default: return state;
    }
};

export default ytReducer;
