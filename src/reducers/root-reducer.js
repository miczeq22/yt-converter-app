import { combineReducers } from 'redux';
import ytReducer from './yt-reducer';

export default combineReducers({
    ytReducer,
});
