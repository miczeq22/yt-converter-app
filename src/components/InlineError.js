import React from 'react';

const InlineError = ({ text }) => {
    return (
        <p style={{ color: `#e84118`, fontSize: `18px`, width: `80%`, margin: 'auto', paddingLeft: '10px' }}>{text}</p>
    );
};

export default InlineError;