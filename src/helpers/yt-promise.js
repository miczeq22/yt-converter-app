const youtubedl = window.require('youtube-dl');

export const videoInfoPromise = url => {
    return new Promise((resolve, reject) => {
        youtubedl.getInfo(url, (error, info) => {
            if (error) {
                reject(error);
            }
            resolve(info);
        });
    });
};
