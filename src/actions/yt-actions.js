import { videoInfoPromise } from '../helpers/yt-promise';

export const SET_VIDEO_INFO = 'SET_VIDEO_INFO';
export const SET_VIDEO_INFO_ERROR = 'SET_VIDEO_INFO_ERROR';
export const RESET_VIDEO_INFO = 'RESET_VIDEO_INFO';

const setVideoInfo = (videoInfo) => {
    return {
        type: SET_VIDEO_INFO,
        videoInfo,
    };
};

const setVideoInfoError = (error) => {
    return {
        type: SET_VIDEO_INFO_ERROR,
        videoInfoError: error,
    };
};

export const resetVideoInfo = () => {
    return {
        type: RESET_VIDEO_INFO,
        videoInfoError: {},
        videoInfo: {},
    };
};

export const getVideoInfo = (url) => {
    return async dispatch => {
        try {
            const videoInfo = await videoInfoPromise(url);
            dispatch(setVideoInfo(videoInfo));
        } catch (error) {
            dispatch(setVideoInfoError(error));
        }

    };
};
