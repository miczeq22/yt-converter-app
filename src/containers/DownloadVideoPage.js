import React, { Component } from 'react';
import SearchBar from './SearchBar';
import VideoThumbnail from './VideoThumbnail';
import { connect } from 'react-redux';
import { getVideoInfo, resetVideoInfo } from '../actions/yt-actions';

class DownloadVideoPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loadingForVideoInfo: false,
            url: '',
            invalidLink: false,
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.renderLoading = this.renderLoading.bind(this);
        this.renderError = this.renderError.bind(this);
    }

    onSubmit = async (url) => {
        this.props.resetVideoInfo();
        this.props.getVideoInfo(url);
        this.setState({ loadingForVideoInfo: true, url });
    }

    renderLoading = () => (
        <h1 style={{ width: `80%`, margin: `auto` }}>Waiting for video info...</h1>
    );

    renderError = () => (
        <h1 style={{ width: `80%`, margin: `auto`, color: `#c23616` }}>Link is invalid</h1>
    );

    render() {
        return (
            <div>
                <SearchBar onSubmit={this.onSubmit} />
                { Object.keys(this.props.videoInfoError).length === 0 ?
                    (this.state.loadingForVideoInfo === true && 
                    (Object.keys(this.props.videoInfo).length > 0 ? <VideoThumbnail url={this.state.url} videoInfo={this.props.videoInfo} /> : this.renderLoading()))
                : this.renderError() }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        videoInfo: state.ytReducer.videoInfo,
        videoInfoError: state.ytReducer.videoInfoError,
    };
};

export default connect(mapStateToProps, { getVideoInfo, resetVideoInfo }) (DownloadVideoPage);
