import React, { Component } from 'react';
import SearchBarForm from '../components/SearchBarForm';

class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit = (url) => {
        this.props.onSubmit(url);
    };

    render() {
        return (
            <div>
                <SearchBarForm onSubmit={this.onSubmit} />
            </div>
        );
    }
}

export default SearchBar;