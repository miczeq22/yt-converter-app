import React, { Component } from 'react';
import '../styles/VideoThumbnail.css';
const fs = window.require('fs');
const youtubedl = window.require('youtube-dl');

class VideoThumbnail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            convertTo: 'mp4',
        };

        this.onChange = this.onChange.bind(this);
        this.onConvert = this.onConvert.bind(this);
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    };

    onConvert = () => {
        var video = youtubedl(this.props.url,
        // Optional arguments passed to youtube-dl.
        [`--format=22`],
        // Additional options can be given for calling `child_process.execFile()`.
        { cwd: __dirname });
        const downloaded = 1;
        video.on('info', function(info) {
            console.log('Download started');
            console.log('filename: ' + info._filename);
           
            // info.size will be the amount to download, add
            var total = info.size + downloaded;
            console.log('size: ' + total);
           
            if (downloaded > 0) {
              // size will be the amount already downloaded
              console.log('resuming from: ' + downloaded);
           
              // display the remaining bytes to download
              console.log('remaining bytes: ' + info.size);
            }
          });
           
          video.pipe(fs.createWriteStream('myvideo.mp4'));
           
          // Will be called if download was already completed and there is nothing more to download.
          video.on('complete', function complete(info) {
            console.log('filename: ' + info._filename + ' already downloaded.');
          });
           
          video.on('end', function() {
            console.log('finished downloading!');
          });

    };

    render() {
        const imgUrl = this.props.videoInfo.thumbnail;
        return (
            <div className="container">
                <div className="description">
                    <div style={{ background: `url(${imgUrl}) center center`,
                    backgroundSize: 'contain', backgroundRepeat: 'no-repeat'}} className="videoImg"></div>
                    <div className="videoDescription">
                        <h1>{this.props.videoInfo.title}</h1>
                        <p>{this.props.videoInfo.description}</p>
                    </div>
                </div>
                <div className="options" value={this.state.convertTo}>
                    <select name="convertTo" onChange={this.onChange}>
                        <option value="mp4" disabled defaultValue>Download As...</option>
                        <option value="mp4">MP4</option>
                        <option value="mp3">MP3</option>
                        <option value="avi">AVI</option>
                    </select>
                    <button onClick={this.onConvert}>Convert</button>
                </div>
            </div>
        );
    }
}

export default VideoThumbnail;