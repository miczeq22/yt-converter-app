import React, { Component } from 'react';
import Navbar from './components/Navbar';
import './styles/App.css';
import { Switch, Route } from 'react-router-dom';
import DownloadVideoPage from './containers/DownloadVideoPage';

class App extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <Switch>
          <Route exact to="/" component={DownloadVideoPage} />
        </Switch>
      </div>
    );
  }
}

export default App;
